# BiLSTM-CRF pytorch 

#### Setup
run **setup.sh** to create a virtual environment and install all the required libraries
run **gen.sh** to create dummy data for the model training

#### Train
run **train.sh** to train pytorch model and get training and testing time for each epoch

#### Details
* To modify the pytorch device(cpu/dpcpp), change the value in the variable 'device' in seqtagpy/utils.py
* Model defination is in seqtagpy/model.py
