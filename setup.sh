python3 -m venv venv
source venv/bin/activate
pip install pip --upgrade
pip install numpy --no-cache-dir
pip install seqeval --no-cache-dir
pip install pyyaml --no-cache-dir

git clone --recursive https://github.com/pytorch/pytorch
cd pytorch
# checkout source code to the specified version
git checkout v1.5.0-rc3
# update submodules for the specified PyTorch version
git submodule sync
git submodule update --init --recursive
cd ../

git clone --recursive https://github.com/intel/intel-extension-for-pytorch
cd intel-extension-for-pytorch
# if you are updating an existing checkout
git submodule sync
git submodule update --init --recursive

cd ../pytorch
git apply ../intel-extension-for-pytorch/torch_patches/dpcpp-v1.5-rc3.patch
python setup.py install

pip install lark-parser hypothesis
cd ../intel-extension-for-pytorch
python setup.py install
