# NOTICE:  All information contained herein is, and remains
# the property of Zen3Tech and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Zen3Tech and its suppliers
# and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Zen3Tech.


from seqtagpy.trainer import train_minibatch, train
import pickle
import torch


with open('train_data.pkl', 'rb') as td:
    train_data = pickle.load(td)

with open('test_data.pkl', 'rb') as td:
    test_data = pickle.load(td)

model = train_minibatch(train_data, test_data, epochs=1, hidden_dim=20, embedding_dim=1024)
#torch.save(model, './model.tor')
