# NOTICE:  All information contained herein is, and remains
# the property of Zen3Tech and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Zen3Tech and its suppliers
# and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Zen3Tech.


import pickle
import numpy as np


def generate_data(num_seqs, min_seq_len, max_seq_len):
    data = []
    for i in range(0,num_seqs, 1):
        print('\r', i, end='')
        seq_len = np.random.randint(min_seq_len, max_seq_len)
        seq = np.float32(np.random.rand(seq_len, 1024))
        tags = np.int16(np.around(np.random.rand(seq_len)))
        tags = ['E' if x==1 else 'O' for x in tags]
        data.append((seq,tags))
#    seqs = np.float16(np.random.rand(num_seqs, 100, 1024))
#    tagss = np.int16(np.around(np.random.rand(num_seqs, 100)))
    print()
    return data

train_data = generate_data(15000, 40, 100)
with open('train_data.pkl', 'wb') as td:
    pickle.dump(train_data, td)

test_data = generate_data(1500, 40, 100)
with open('test_data.pkl', 'wb') as td:
    pickle.dump(test_data, td)
