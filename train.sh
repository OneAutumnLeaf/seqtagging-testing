source venv/bin/activate
export OMP_NUM_THREADS=4
export KMP_AFFINITY=granularity=fine,verbrose,compact,1,0
export KMP_BLOCKTIME=1
export MKLDNN_VERBROSE=1
CUDA_VISIBLE_DEVICES=-1 python tds.py
