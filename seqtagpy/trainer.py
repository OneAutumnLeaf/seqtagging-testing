# NOTICE:  All information contained herein is, and remains
# the property of Zen3Tech and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Zen3Tech and its suppliers
# and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Zen3Tech.


import torch
import torch.optim as optim
import numpy as np
#import torch.multiprocessing as mp

torch.manual_seed(1)

from seqtagpy.models import BiLSTM_CRF
from seqtagpy.utils import *
from seqeval.metrics import f1_score
from time import time


def split(a, n):
    k, m = divmod(len(a), n)
    return (a[i * k + min(i, m):(i + 1) * k + min(i + 1, m)] for i in range(n))


def split_batch(a, n):
    k, m = divmod(len(a), n)
    rl = []
    for i in range(k):
        rl.append(a[:n])
        del a[:n]
    if m!=0:
        rl.append(a[:m])
        del a[:m]
    return rl


def padd(x, y, tag_to_ix):
    assert len(x)==len(y)
    ly = [len(a) for a in y]
    ml = max(ly)
    rx = np.zeros((len(x), ml, len(x[0][0])), dtype=type(x[0][0][0]))
    ry = np.full((len(x), ml), tag_to_ix[PAD_TAG], dtype=np.uint8)
    rm = np.zeros((len(x), ml), dtype=np.uint8)
    for i in range(len(x)):
        rm[i, :(ly[i]-1)] = 1
        rm[i, (ly[i]-1)] = 2
        rx[i, :ly[i]] = x[i][:ly[i]]
        ry[i, :ly[i]] = [tag_to_ix[a] for a in y[i]]
    return rx, ry, rm


def class_count(td, tag_to_ix):
    tags_count = dict()
    for tag in tag_to_ix:
        tags_count[tag] = 0
    for x, y in td:
        for tag in y:
            tags_count[tag] += 1
        tags_count[START_TAG] += 1
        tags_count[STOP_TAG] += 1
    tc = 0
    for tag in tags_count:
        tc += tags_count[tag]
    for tag in tags_count:
        tags_count[tag] = tags_count[tag]/tc
    return tags_count


def tags_to_ixs(td, tag_to_ix):
    new_td = []
    for x, y in td:
        new_td.append((x, [tag_to_ix[z] for z in y]))


def train_minibatch(training_data, validation_data, epochs = 5, embedding_dim = 100, hidden_dim = 4, optimizer = "SGD"):
    tag_to_ix = {}
    for sentence, tags in training_data:
        for tag in tags:
            if tag not in tag_to_ix:
                tag_to_ix[tag] = len(tag_to_ix)
    tag_to_ix[START_TAG] = len(tag_to_ix)
    tag_to_ix[STOP_TAG] = len(tag_to_ix)
    tag_to_ix[PAD_TAG] = len(tag_to_ix)
    print(tag_to_ix)
    np = 64
    training_data_split = split_batch(training_data, np)
    training_data.clear()
    print(len(training_data_split))
    print(len(training_data_split[0]))
    model = BiLSTM_CRF(tag_to_ix, embedding_dim, hidden_dim).to(device)
    optimizer = optim.SGD(model.parameters(), lr=0.002, weight_decay=1e-4)
    while training_data_split:
        training_batch = training_data_split.pop()
        inpt, outpt, maskss = padd([x[0] for x in training_batch], [x[1] for x in training_batch], tag_to_ix)
        #print(outpt)
        inpt = torch.tensor(inpt, device=device)
        outpt = torch.tensor(outpt, dtype=torch.long, device=device)
        maskss = torch.tensor(maskss, dtype=torch.long, device=device)
        training_data.append((inpt, outpt, maskss))
    print(model.transitions)
    for epoch in range(epochs):
        print(epoch)
        st = time()
        for inpt, outpt, maskss in training_data:
            optimizer.zero_grad()
            loss = model.neg_log_likelihood_batch(inpt, outpt, maskss)
            loss.backward()
            optimizer.step()
        mt = time()
        test(model, 'E', validation_data)
        et = time()
        print('training time', (mt-st))
        print('testing time', (et-mt))
    print(model.transitions)

    return model


def train(training_data, validation_data, epochs = 5, embedding_dim = 100, hidden_dim = 4, optimizer = "SGD"):
    tag_to_ix = {}
    for sentence, tags in training_data:
        for tag in tags:
            if tag not in tag_to_ix:
                tag_to_ix[tag] = len(tag_to_ix)
    tag_to_ix[START_TAG] = len(tag_to_ix)
    tag_to_ix[STOP_TAG] = len(tag_to_ix)
    print(tag_to_ix)
    np = 64
    #tp = class_count(training_data, tag_to_ix)
    #training_data = tags_to_ixs(training_data, tag_to_ix)
    training_data_split = list(split_batch(training_data, np))
    training_data.clear()
    print(len(training_data_split))
    print(len(training_data_split[0]))
    model = BiLSTM_CRF(tag_to_ix, embedding_dim, hidden_dim).to(device)
    optimizer = optim.SGD(model.parameters(), lr=0.002, weight_decay=1e-4)
    #test(model, 'E-sent', validation_data)
    print(model.transitions)
    for epoch in range(epochs):
        print(epoch)
        for training_batch in training_data_split:
            optimizer.zero_grad()
            lei = len(training_batch)
            loss = 0
            for ti in training_batch:
                li = len(ti[1])
                seq = torch.tensor(ti[0][:li], device=device)
                tags = torch.tensor([tag_to_ix[tag] for tag in ti[1]], device=device)
                #tags = torch.tensor(ti[1], device=device)
                loss += model.neg_log_likelihood(seq, tags)  #/lei
            loss.backward()
            optimizer.step()
        test(model, 'E-sent', validation_data)
    print(model.transitions)

    return model

def tester(model, validation_data):
    print(model.transitions)
    test(model, 'E-sent', validation_data)

    return model


def test(model, test_tag, testing_data):
    cm = {"TP":0, "TN":0, "FP": 0, "FN": 0}
    ix_to_tag = {model.tag_to_ix[tag]:tag for tag in model.tag_to_ix}
    # Check predictions after training
    y_true = []
    y_pred = []
    with torch.set_grad_enabled(False):
        for sentence, tags in testing_data:
            #precheck_sent = prepare_sequence(training_data[0][0], word_to_ix)
            li = len(tags)
            sentence_in = torch.tensor(sentence[:li], device=device)
            test_tags = model(sentence_in)[1]
            test_tags = [ix_to_tag[x] for x in test_tags]
            y_true.append(tags)
            y_pred.append(test_tags)
            for tru, pred in zip(tags, test_tags):
                #if True:
                #tru, pred = tags[15], test_tags[15]
                if tru==pred:
                    if tru==test_tag:
                        cm["TP"]+=1
                    else:
                        cm["TN"]+=1
                else:
                    if tru==test_tag:
                        cm["FN"]+=1
                    else:
                        cm["FP"]+=1
        print(cm)
        f1_sco = f1_score(y_true, y_pred)
        print("F1 Score", f1_sco)


def train_batch(model, ique, oque, lock):
    inp = ique.get()
    while inp:
        lock.acquire()
        try:
            sentence, tags = inp
            sentence_in = torch.tensor(sentence, dtype=torch.float)
            targets = torch.tensor([model.tag_to_ix[t] for t in tags], dtype=torch.long)
            #print(sentence_in)
            # Step 3. Run our forward pass.
            loss = model.neg_log_likelihood(sentence_in, targets)
            # Step 4. Compute the loss, gradients, and update the parameters by
            # calling optimizer.step()
            loss.backward()
        except:
            print('lock error')
        finally:
            lock.release()
        oque.put(True)
        inp = ique.get()
    print('done')

def train_process(model, index, training_data):  #, optimizer):
    print(len(training_data), index)
    i = 1
    optimizer = optim.SGD(model.parameters(), lr=0.01, weight_decay=1e-4)

    for sentence, tags in training_data:
        print(i)
        i+=1
        # Step 1. Remember that Pytorch accumulates gradients.
        # We need to clear them out before each instance
        model.zero_grad()
        # Step 2. Get our inputs ready for the network, that is,
        # turn them into Tensors of word indices.
        sentence_in = torch.tensor(sentence, dtype=torch.float)
        targets = torch.tensor([model.tag_to_ix[t] for t in tags], dtype=torch.long)
        #print(sentence_in)
        # Step 3. Run our forward pass.
        loss = model.neg_log_likelihood(sentence_in, targets)
        # Step 4. Compute the loss, gradients, and update the parameters by
        # calling optimizer.step()
        loss.backward()
        optimizer.step()
    #print(training_data[0][1])
    print(index, "done")


def grain(x, y, vx, vy, embedding_dim=100, hidden_dim=4, epochs=3, batch_size=64, num_workers=12, optimizer="SGD"):
    START_TAG = "<START>"
    STOP_TAG = "<STOP>"
    tag_to_ix = {}
    padd(x, y)
    padd(vx, vy)
    for tags in y:
        for tag in tags:
            if tag not in tag_to_ix:
                tag_to_ix[tag] = len(tag_to_ix)
    tag_to_ix[START_TAG] = len(tag_to_ix)
    tag_to_ix[STOP_TAG] = len(tag_to_ix)
    print(tag_to_ix)
    y = [[tag_to_ix[tag] for tag in tags] for tags in y]
    vy = [[tag_to_ix[tag] for tag in tags] for tags in vy]
    ix_to_tag = {tag_to_ix[tag]:tag for tag in tag_to_ix}
    model = BiLSTM_CRF(tag_to_ix, embedding_dim, hidden_dim).to(device)
    #model.share_memory()
    optimizer = optim.SGD(model.parameters(), lr=0.01, weight_decay=1e-4)

    params = {'batch_size': batch_size,
              'shuffle': True,
              'num_workers': num_workers}
    training_set = Dataset(x, y, tag_to_ix)
    training_generator = torch.utils.data.DataLoader(training_set, **params)

    validation_set = Dataset(vx, vy, tag_to_ix)
    validation_generator = torch.utils.data.DataLoader(validation_set, **params)

    ttt = 'E-sent'

    for epoch in range(epochs):
        print(epoch)
        for batch_xs, batch_ys in training_generator:
            #print(len(batch_ys), end=' ')
            model.zero_grad()
            loss = torch.zeros(1, device=device)
            for seq, tag in zip(batch_xs, batch_ys):
                loss += model.neg_log_likelihood(seq, tag)
            loss.backward()
            optimizer.step()
        print()
        with torch.set_grad_enabled(False):
            cm = {"TP": 0, "TN": 0, "FP": 0, "FN": 0}
            for batch_xs, batch_ys in validation_generator:
                for seq, tag in zip(batch_xs, batch_ys):
                    test_tag = model(seq)[1]
                    for tru, pred in zip(tag, test_tag):
                        if tru==pred:
                            if tru==tag_to_ix[ttt]:
                                cm["TP"]+=1
                            else:
                                cm["TN"]+=1
                        else:
                            if tru==tag_to_ix[ttt]:
                                cm["FP"]+=1
                            else:
                                cm["FP"]+=1
            print(cm)
            precision = cm["TP"]/(cm["TP"]+cm["FP"])
            recall = cm["TP"]/(cm["TP"]+cm["FN"])
            print("precision", precision)
            print("recall", recall)
            if precision+recall != 0:
                f1_score = (2*precision*recall)/(precision+recall)
                print('F1 Score', f1_score)
    return model
