# NOTICE:  All information contained herein is, and remains
# the property of Zen3Tech and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Zen3Tech and its suppliers
# and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Zen3Tech.


import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

from seqtagpy.utils import *

torch.manual_seed(1)


class BiLSTM_CRF(nn.Module):

    def __init__(self, tag_to_ix, embedding_dim, hidden_dim):
        super(BiLSTM_CRF, self).__init__()
        self.embedding_dim = embedding_dim
        self.hidden_dim = hidden_dim
        self.tag_to_ix = tag_to_ix
        self.tagset_size = len(tag_to_ix)

        self.lstm = nn.LSTM(embedding_dim, hidden_dim // 2, num_layers=1, bidirectional=True, batch_first=True).to(device)

        # Maps the output of the LSTM into tag space.
        self.hidden2tag = nn.Linear(hidden_dim, self.tagset_size).to(device)

        self.hidden = self.init_hidden()

        # Matrix of transition parameters.  Entry i,j is the score of
        # transitioning *to* i *from* j.
        self.transitions = nn.Parameter(torch.randn(self.tagset_size, self.tagset_size, device=device)).to(device)

        # These two statements enforce the constraint that we never transfer
        # to the start tag and we never transfer from the stop tag
        self.transitions.data[tag_to_ix[START_TAG], :] = -10000.
        self.transitions.data[:, tag_to_ix[STOP_TAG]] = -10000.
        
        if PAD_TAG in tag_to_ix:
            self.transitions.data[tag_to_ix[PAD_TAG], :] = -10000.
            self.transitions.data[:, tag_to_ix[PAD_TAG]] = -10000.
            self.transitions.data[tag_to_ix[PAD_TAG], tag_to_ix[PAD_TAG]] = 0.0
            self.transitions.data[tag_to_ix[STOP_TAG], tag_to_ix[PAD_TAG]] = 0.0

    def init_hidden(self):
        return (torch.randn(2, 1, self.hidden_dim // 2, device=device),
                torch.randn(2, 1, self.hidden_dim // 2, device=device))

    def init_hidden_batch(self, bat_siz):
        return (torch.randn(2, bat_siz, self.hidden_dim // 2, device=device),
                torch.randn(2, bat_siz, self.hidden_dim // 2, device=device))

    def _get_lstm_features(self, sentence):
        self.hidden = self.init_hidden()
        embeds = sentence.unsqueeze(0)
        lstm_out, self.hidden = self.lstm(embeds, self.hidden)
        lstm_out = lstm_out.view(len(sentence), self.hidden_dim)
        lstm_feats = self.hidden2tag(lstm_out)
        return lstm_feats

    def _get_lstm_features_batch(self, sentences, sent_lens):
        bat_siz, _, _ = sentences.shape
        self.hidden = self.init_hidden_batch(bat_siz)
        # to cpu required to use pack_padded_sequence when ipex is enabled
        embeds = pack_padded_sequence(sentences.to('cpu'), sent_lens, batch_first=True, enforce_sorted=False)
        #embeds = pack_padded_sequence(sentences, sent_lens, batch_first=True, enforce_sorted=False)
        lstm_out, self.hidden = self.lstm(embeds, self.hidden)
        lstm_out, _ = pad_packed_sequence(lstm_out, batch_first=True)
        lstm_feats = self.hidden2tag(lstm_out)
        return lstm_feats

    def _forward_alg2(self, feats):
        # Do the forward algorithm to compute the partition function
        sn = self.tagset_size
        init_alphas = self.transitions[:, self.tag_to_ix[START_TAG]] + feats[0]
        # Wrap in a variable so that we will get automatic backprop
        forward_var = init_alphas
        seq_len, _ = feats.shape

        # Iterate through the sentence
        for i in range(1, seq_len):
            mes = feats[i].view(sn, 1).expand(sn, sn)
            mfv = forward_var.expand(sn, sn)
            mntv = mfv+mes+self.transitions
            ans = torch.logsumexp(mntv, 1)
            forward_var = ans
        terminal_var = forward_var + self.transitions[self.tag_to_ix[STOP_TAG]]
        alpha = torch.logsumexp(terminal_var, 0)
        return alpha

    def _forward_alg2_batch(self, featss, maskss):
        # Do the forward algorithm to compute the partition function
        sn = self.tagset_size
        init_alphas = self.transitions[:, self.tag_to_ix[START_TAG]].unsqueeze(0) + featss[:, 0]
        # Wrap in a variable so that we will get automatic backprop
        forward_var = init_alphas
        bat_siz, seq_len, _ = featss.shape
        optran = self.transitions.unsqueeze(0).expand(bat_siz, -1, -1)
        maskss = (maskss+1)//2

        # Iterate through the sentence
        for i in range(1, seq_len):
            mes = featss[:,i].unsqueeze(2).expand(bat_siz, sn, sn)
            mfv = forward_var.unsqueeze(1).expand(bat_siz, sn, sn)
            mntv = mfv+mes+optran
            ans = torch.logsumexp(mntv, 2)
            masks = (maskss[:, i]).unsqueeze(-1)
            forward_var = ans*masks + forward_var*(1-masks)
        terminal_var = forward_var + self.transitions[self.tag_to_ix[STOP_TAG]].unsqueeze(0)
        alpha = torch.logsumexp(terminal_var, 1)
        return alpha

    def _viterbi_decode2(self, feats):
        backpointers = []
        sn = self.tagset_size

        # Initialize the viterbi variables in log space
        forward_var = self.transitions[:, self.tag_to_ix[START_TAG]] + feats[0]
        seq_len, _ = feats.shape

        for i in range(1, seq_len):
            mfv = forward_var.expand(sn, sn)
            mntv = self.transitions+mfv
            mbti = torch.max(mntv, 1)
            forward_var = mbti[0]+feats[i]
            backpointers.append(mbti[1])

        # Transition to STOP_TAG
        terminal_var = forward_var + self.transitions[self.tag_to_ix[STOP_TAG]]
        best_tag_id = torch.argmax(terminal_var).item()
        path_score = terminal_var[best_tag_id]

        # Follow the back pointers to decode the best path.
        best_path = [best_tag_id]
        for bptrs_t in reversed(backpointers):
            best_tag_id = bptrs_t[best_tag_id].item()
            best_path.append(best_tag_id)
        best_path.reverse()
        #print("after vitribe decoder", time())
        return path_score, best_path

    def _score_sentence(self, feats, tags):
        # Gives the score of a provided tag sequence
        score = torch.zeros(1, device=device)
        tags = torch.cat([torch.tensor([self.tag_to_ix[START_TAG]], dtype=torch.long, device=device), tags])
        for i, feat in enumerate(feats):
            score = score + self.transitions[tags[i + 1], tags[i]] + feat[tags[i + 1]]
        score = score + self.transitions[self.tag_to_ix[STOP_TAG], tags[-1]]
        return score

    def _score_sentence_batch(self, featss, tagss, maskss):
        # Gives the score of a provided tag sequence
        bat_siz, seg_len, _ = featss.shape
        prev_tags = maskss.argmax(1)
        prev_tags = tagss.gather(1, prev_tags.unsqueeze(1)).squeeze()
        score = self.transitions[self.tag_to_ix[STOP_TAG], prev_tags]
        prev_tags = torch.tensor([self.tag_to_ix[START_TAG]]*bat_siz, device=device)
        maskss = (maskss+1)//2
        for i in range(seg_len):
            curr_tags = tagss[:, i]
            masks = maskss[:, i]
            upsco = self.transitions[curr_tags, prev_tags] + featss[:, i].gather(1, curr_tags.unsqueeze(1)).squeeze()
            upsco = upsco * masks
            score = score + upsco
            prev_tags = curr_tags
        return score

    def neg_log_likelihood(self, sentence, tags):
        sentence = sentence.to(device)
        tags = tags.to(device)
        feats = self._get_lstm_features(sentence)
        forward_score = self._forward_alg2(feats)
        gold_score = self._score_sentence(feats, tags)
        return forward_score - gold_score

    def neg_log_likelihood_batch(self, sentences, tagss, maskss=None):
        if maskss is None:
            maskss = torch.ones(tagss.shape)
            maskss[:, -1] = 2
        sentences = sentences.to(device)
        tagss = tagss.to(device)
        maskss = maskss.to(device)
        featss = self._get_lstm_features_batch(sentences, maskss.argmax(1)+1)
        forward_scores = self._forward_alg2_batch(featss, maskss)
        gold_scores = self._score_sentence_batch(featss, tagss, maskss)
        return torch.sum(forward_scores - gold_scores)

    def forward(self, sentence):
        sentence = sentence.to(device)
        # dont confuse this with _forward_alg above.
        # Get the emission scores from the BiLSTM
        lstm_feats = self._get_lstm_features(sentence)
        #print("after lstm computation", time())

        # Find the best path, given the features.
        score, tag_seq = self._viterbi_decode2(lstm_feats)
        return score, tag_seq
