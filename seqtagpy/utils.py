# NOTICE:  All information contained herein is, and remains
# the property of Zen3Tech and its suppliers,
# if any.  The intellectual and technical concepts contained
# herein are proprietary to Zen3Tech and its suppliers
# and are protected by trade secret or copyright law.
# Dissemination of this information or reproduction of this material
# is strictly forbidden unless prior written permission is obtained
# from Zen3Tech.


import torch
import intel_pytorch_extension as ipex

torch.manual_seed(1)

use_cuda = torch.cuda.is_available()

device = ipex.DEVICE
#device = torch.device("cuda" if use_cuda else "cpu")
START_TAG = '<START>'
STOP_TAG = '<STOP>'
PAD_TAG = '<PAD>'


class Dataset(torch.utils.data.Dataset):
    def __init__(self, seqs, tagss, tag_to_ix):
        self.tag_to_ix=tag_to_ix
        self.seqs=torch.tensor(seqs)
        self.tagss=torch.tensor(tagss)
    def __len__(self):
        return len(self.seqs)
    def __getitem__(self, index):
        x=self.seqs[index]
        y=self.tagss[index]
        return x, y


def argmax(vec):
    # return the argmax as a python int
    _, idx = torch.max(vec, 1)
    return idx.item()


def prepare_sequence(seq, to_ix):
    idxs = [to_ix[w] for w in seq]
    return torch.tensor(idxs, dtype=torch.long, device=device)


# Compute log sum exp in a numerically stable way for the forward algorithm
def log_sum_exp(vec):
    max_score = vec[0, argmax(vec)]
    max_score_broadcast = max_score.view(1, -1).expand(1, vec.size()[1])
    return max_score + \
        torch.log(torch.sum(torch.exp(vec - max_score_broadcast)))
